﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeConstruction
{
    public class Roof : IPart
    {
        public bool IsBuilded { get; set; }
        public Roof(bool status)
        {
            IsBuilded = status;
        }
    }
}
