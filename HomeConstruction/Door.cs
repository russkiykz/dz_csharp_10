﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeConstruction
{
    public class Door : IPart
    {
        public bool IsBuilded { get; set; }
        public Door(bool status)
        {
            IsBuilded = status;
        }
    }
}
