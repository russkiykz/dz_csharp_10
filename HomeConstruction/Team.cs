﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeConstruction
{
    public class Team
    {
        public Worker Worker { get; set; }
        public TeamLeader TeamLeader { get; set; }
        
        public void BuildingHouse()
        {
            House house = new House();
            TeamLeader = new TeamLeader();
            Worker = new Worker();
            int count = 0;
            while (true)
            {
                count++;
                Console.WriteLine($"\nЭтап {count}");
                Worker.MakeWork(house);
                TeamLeader.MakeWork(house);
                if (house.Roof.IsBuilded == true)
                {
                    break;
                }
            }
            
        }

    }
}
