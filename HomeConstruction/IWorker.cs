﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeConstruction
{
    public interface IWorker
    {
        public bool MakeWork(House house);
    }
}
