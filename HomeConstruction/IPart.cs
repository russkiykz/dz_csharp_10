﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeConstruction
{
    public interface IPart
    {
        public bool IsBuilded { get; set; }
    }
}
