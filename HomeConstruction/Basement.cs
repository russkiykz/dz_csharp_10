﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeConstruction
{
    public class Basement : IPart
    {
        public bool IsBuilded { get; set; }
        public Basement(bool status)
        {
            IsBuilded = status;
        }
    }
}
