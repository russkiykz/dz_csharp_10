﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeConstruction
{
    public class House
    {
        public Basement Basement { get; set; }
        public Walls[] Walls { get; set; }
        public Door Door { get; set; }
        public Window[] Windows { get; set; }
        public Roof Roof { get; set; }

        public House()
        {
            Basement = new Basement(false);
            Walls = new Walls[4] {new Walls(false), new Walls(false), new Walls(false), new Walls(false) };
            Door = new Door(false);
            Windows = new Window[4] { new Window(false), new Window(false), new Window(false), new Window(false) };
            Roof = new Roof(false);

        }
    }
}
