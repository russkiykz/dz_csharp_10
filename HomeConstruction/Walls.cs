﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeConstruction
{
    public class Walls : IPart
    {
        public bool IsBuilded { get; set; }
        public Walls(bool status)
        {
            IsBuilded = status;
        }
    }
}
