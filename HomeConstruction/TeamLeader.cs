﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeConstruction
{
    public class TeamLeader : IWorker
    {
        public bool MakeWork(House house)
        {
            switch (house.Basement.IsBuilded)
            {
                case true:
                    Console.WriteLine("Фундамент - заложен");
                    break;
                case false:
                    Console.WriteLine("Фундамент - не заложен");
                    break;
            }
            for(int i = 0; i < house.Walls.Length; i++)
            {
                switch (house.Walls[i].IsBuilded)
                {
                    case true:
                        Console.WriteLine($"Стена {i+1} - построена");
                        break;
                    case false:
                        Console.WriteLine($"Стена {i + 1} - не построена");
                        break;
                }
            }
            switch (house.Door.IsBuilded)
            {
                case true:
                    Console.WriteLine("Дверь установлена");
                    break;
                case false:
                    Console.WriteLine("Дверь не установлена");
                    break;
            }
            for (int i = 0; i < house.Windows.Length; i++)
            {
                switch (house.Windows[i].IsBuilded)
                {
                    case true:
                        Console.WriteLine($"Окно {i + 1} - установлено");
                        break;
                    case false:
                        Console.WriteLine($"Окно {i + 1} - не установлено");
                        break;
                }
            }
            switch (house.Roof.IsBuilded)
            {
                case true:
                    Console.WriteLine("Крыша установлена");
                    Console.WriteLine("\nДля продолжения нажмите любую клавишу...");
                    Console.ReadLine();
                    Console.Clear();
                    Console.WriteLine("Дом построен");
                    Console.WriteLine(@"
                           (   )
                          (    )
                           (    )
                          (    )
                            )  )
                           (  (                  /\
                            (_)                 /  \  /\
                    ________[_]________      /\/    \/  \
           /\      /\        ______    \    /   /\/\  /\/\
          /  \    //_\       \    /\    \  /\/\/    \/    \
   /\    / /\/\  //___\       \__/  \    \/
  /  \  /\/    \//_____\       \ |[]|     \
 /\/\/\/       //_______\       \|__|      \
/      \      /XXXXXXXXXX\                  \
        \    /_I_II  I__I_\__________________\
               I_I|  I__I_____[]_|_[]_____I
               I_II  I__I_____[]_|_[]_____I
               I II__I  I     XXXXXXX     I
            ~~~~~'   '~~~~~~~~~~~~~~~~~~~~~~~~");
                    ;
                    break;
                case false:
                    Console.WriteLine("Крыша не установлена");
                    break;
            }
            Console.WriteLine("\nДля продолжения нажмите любую клавишу...");
            Console.ReadLine();
            return true;
        }
    }
}
