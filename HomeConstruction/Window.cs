﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeConstruction
{
    public class Window : IPart
    {
        public bool IsBuilded { get; set; }
        public Window(bool status)
        {
            IsBuilded = status;
        }
    }
}
